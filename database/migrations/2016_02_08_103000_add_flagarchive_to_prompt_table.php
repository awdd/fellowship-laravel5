<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagarchiveToPromptTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('prompts', function(Blueprint $table)
		{
      $table->boolean('flag_archive')->nullable;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('prompts', function(Blueprint $table)
		{
			if(Schema::hasColumn('flag_archive'))
      {
        $table->dropColumn('flag_archive');
      }
		});
	}

}
