<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWritersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('writers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('name', 50);
      $table->char('slug', 50);
			$table->char('link_facebook', 250)->nullable;
			$table->boolean('flag_goat')->nullable;
			$table->enum('flag_nsfw', array('SFW', 'NSFW', 'Either'))->nullable;
			$table->enum('flag_au', array('No', 'AU', 'AU World', 'AU Time'))->nullable;
			$table->enum('flag_dlc', array('No', 'All', 'Jaws of Hakkon', 'Descent', 'Trespasser'))->nullable;
      $table->enum('flag_status', array('active', 'inactive', 'archive'));
			$table->text('notes')->nullable;
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('writers');
	}

}
