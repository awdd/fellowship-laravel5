<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromptsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('prompts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->char('name', 50);
			$table->text('prompt');
			$table->enum('flag_nsfw', array('SFW', 'NSFW', 'Either'))->nullable;
			$table->enum('flag_au', array('No', 'AU', 'AU World', 'AU Time'))->nullable;
			$table->date('date_completed')->default('0000-00-00');
			$table->char('link_prompt', 250)->nullable();
			$table->integer('challenges_id')->unsigned()->nullable();
			$table->integer('writer_id')->unsigned()->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('prompts');
	}

}
