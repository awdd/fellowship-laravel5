<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChallengesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('challenges', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
      $table->string('slug', 100);
      $table->date('date_start')->nullable();
      $table->date('date_end')->nullable();
      $table->enum('flag_status', ['prep', 'prompt_request', 'writing', 'complete', 'archive']);
      $table->enum('flag_type', ['seasonal', 'weekly', 'monthly', 'random']);
      $table->mediumText('description')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('challenges');
	}

}
