<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagpostToPromptTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('prompts', function(Blueprint $table)
		{
      $table->boolean('flag_posted')->nullable;
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('prompts', function(Blueprint $table)
		{
			if(Schema::hasColumn('flag_posted'))
      {
        $table->dropColumn('flag_posted');
      }
		});
	}

}
