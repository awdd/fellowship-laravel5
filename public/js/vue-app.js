new Vue({
  el: '#prompts',
  
  data: {

  },
  
  computed: {

  },
  
  ready: function() {
    this.fetchMessages();
  },
  
  methods: {
    fetchMessages: function() {
      this.$http.get('/apipr', function(prompts) {
        this.$set('prompts', prompts);
      });
    },
  },
  
});
