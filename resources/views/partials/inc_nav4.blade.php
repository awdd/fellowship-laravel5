<nav class="navbar navbar-dark bg-primary">
  <a class="navbar-brand" href="/">Fellowship <small>of Cullenite Writers</small></a>
  <button class="navbar-toggler hidden-sm-up pull-right" type="button" data-toggle="collapse" data-target="#exCollapsingNavbar1">
    &#9776;
  </button>
  <div class="collapse navbar-toggleable-xs" id="exCollapsingNavbar1">

    <ul class="nav nav-pills navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/challenges"><span class="icon icon-challenge"></span> Challenges</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/prompts"><span class="icon icon-prompts"></span> Prompts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/writers"><span class="icon icon-writer"></span> Writers</a>
      </li>
    </ul>
  </div>
</nav>
