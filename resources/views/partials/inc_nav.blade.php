<nav>
  <ul class="nav nav-pills pull-right">
    <li><a href="/">Home</a></li>
    <li role="presentation" class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Manage <span class="caret"></span</a>
      <ul class="dropdown-menu">
        <li><a href="/challenges">Challenges</a></li>
        <li><a href="/writers">Writers</a></li>
        <li><a href="/prompts">Prompts</a></li>
      </ul>
    </li>
    <li><a href="http://www.facebook.com/groups/763050627145195">Contact</a></li>
  </ul>
</nav>
<div class="clearfix"></div>
