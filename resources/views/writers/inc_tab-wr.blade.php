{!! Form::model($writer, ['method' => 'PATCH', 'action' => ['WritersController@update', $writer->id], 'class' => 'form form-horizontal']) !!}
<div class="col-sm-9">
  <fieldset>
    <!-- Status Form Field -->
    <div class="form-group row">
      {!! Form::label('Status', 'Status:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_status', $writer->getStatus(), $writer->flag_status, ['class' => 'form-control']) !!}
      </div>
    </div>

    <div class="form-group row">
      {!! Form::label('name', 'Name:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control col-sm-8', 'id' => 'name', 'placeholder' => 'Enter a writer\'s name']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('link_facebook', 'Facebook Link:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8 input-group">
        {!! Form::text('link_facebook', null, ['class' => 'form-control', 'id' => 'link_facebook', 'placeholder' => 'Enter the writer\'s Facebook link']) !!}
        <span class="input-group-addon"><a href="{{ $writer->link_facebook }}" target="_blank"><span class="icon icon-facebook"></span></a></span>
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('flag_goat', 'Goat:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_goat', $writer->getGoat(), $writer->flag_goat, ['class' => 'form-control col-sm-8 c-select']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('flag_nsfw', 'NSFW:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_nsfw', $writer->getNsfw(), $writer->flag_nsfw, ['class' => 'form-control col-sm-8 c-select']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('flag_au', 'AU:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_au',$writer->getAu(), $writer->flag_au, ['class' => 'form-control col-sm-8 c-select']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('flag_dlc', 'DLC:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_dlc', $writer->getDlc(), $writer->flag_dlc, ['class' => 'form-control col-sm-8 c-select']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('notes', 'Notes:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('notes', null, ['class' => 'form-control col-sm-8']) !!}
      </div>
    </div>
  </fieldset>
  @include('writers.inc_writerfanfic')

    <!-- Add A New Writer Form Submit Button -->
  <div class="form-group row">
    <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Update Writer', ['class' => 'btn btn-primary form-control']) !!}</div>
  </div>

</div>
<div class="col-sm-3">
  <div class="form-group row">
    {!! Form::label('challenges_list', 'Challenges:', ['class' => '']) !!}
      {!! Form::select('challenges_list[]', $challenges_list, null, ['class' => 'form-control', 'id' => 'challenges_list', 'multiple']) !!}
  </div>
</div>
{!! Form::close()  !!}


@section('footer')
  @parent
  <script type="text/javascript">
    $('#challenges_list').select2(
      {
        placeholder: 'Select a challenge'
      }
    );
  </script>
@endsection
