<table class="table table-striped table-hover table-responsive">
  <thead>
  <tr>
    <th></th>
    <th>Name</th>
    <th></th>
    <th align="center"><span class="icon icon-challenge"></span></th>
    <th align="center"><span class="icon icon-prompts"></span></th>
    <th align="center">Goat</th>
    <th align="center">NSFW</th>
    <th align="center">AU</th>
    <th align="center">DLC</th>
    <th class="visible-md">Notes</th>
    <th>Actions</th>
  </tr>
  </thead>
  <tbody>
  @foreach( $writers as $writer )
    <tr class="status_{{ $writer->flag_status }}">
      <td class="status_{{ $writer->flag_status }}"><span class="icon icon-{{ $writer->flag_status }}" title="{{ $writer->flag_status }}"></span></td>
      <td class="status_{{ $writer->flag_status }}">
        <a href="/writers/{{ $writer->id }}/edit">{{ $writer->name }}</a>
      </td>
      <td class="status_{{ $writer->flag_status }}">
        <a href="{{ $writer->link_facebook }}" target="_blank" class=""><span class="icon icon-facebook" title="Facebook"></span></a>
      </td>
      <td class="status_{{ $writer->flag_status }} center" align="center">
        <span class="label label-primary label-pill " title="Challenges Participated In">{{ count( $writer->challenges ) }}</span>
      </td>
      <td class="status_{{ $writer->flag_status }} center" align="center">
        <span class="label label-info label-pill " title="Prompts Assigned">{{ count( $writer->prompts ) }}</span>
      </td>
      <td class="status_{{ $writer->flag_status }} center" align="center">
        @if( $writer->flag_goat == 1)<span class="icon icon-goat" title="Goat"></span>@endif
      </td>
      <td class="status_{{ $writer->flag_status }} center" align="center">
        <span class="icon icon-{{ $writer->flag_nsfw }}" title="{{ $writer->flag_nsfw }}"></span>
      </td>
      <td class="status_{{ $writer->flag_status }} center" align="center">
        {{ $writer->flag_au }}
      </td>
      <td class="status_{{ $writer->flag_status }} center" align="center">
        {{ $writer->flag_dlc }}
      </td>
      <td class="status_{{ $writer->flag_status }} visible-md">
        {{ $writer->notes }}
      </td>
      <td
        @if( $writer->flag_status == 'active') class="status_archive"
        @elseif( $writer->flag_status == 'inactive') class="status_active"
        @endif
      >
        @if($writer->flag_status != 'archive')
        {!! Form::model($writer, ['method' => 'DELETE', 'action' => ['WritersController@destroy', $writer->id]]) !!}
          <!-- Archive Form Submit Button -->
        <div>
          {!! Form::submit('Archive', ['class' => 'btn btn-sm btn-primary btn-danger form-control']) !!}
        </div>
        {!! Form::close() !!}
        @endif
      </td>
    </tr>
  @endforeach
  </tbody>
  <tfoot>

  </tfoot>
</table>
