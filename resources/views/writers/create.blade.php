@extends('layout')

@section('content')
    <h1>Add a New Writer</h1>

    @include('errors.error_list')

    {!! Form::open(['url' => 'writers', 'class' => 'form form-horizontal']) !!}

    <div class="form-group row">
      {!! Form::label('name', 'Name:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control col-sm-8', 'id' => 'name', 'placeholder' => 'Enter a writer\'s name', 'required']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('link_facebook', 'Facebook Link:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('link_facebook', null, ['class' => 'form-control', 'id' => 'link_facebook', 'placeholder' => 'Enter the writer\'s Facebook link', 'required']) !!}
      </div>
    </div>
    <div class="form-group row">
      {!! Form::label('notes', 'Notes:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('notes', null, ['class' => 'form-control col-sm-8']) !!}
      </div>
    </div>

    <!-- Add A New Writer Form Submit Button -->
    <div class="form-group row">
      <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Add A New Writer', ['class' => 'btn btn-primary form-control']) !!}</div>
    </div>

    {!! Form::close()  !!}
@endsection
