@extends('layout')

@section('content')
    <h1>Edit Writer {{ $writer->name }}</h1>

    <div class="tab-content">
      <ul class="nav nav-tabs" role="tablist" id="challengeTab">
        <li class="nav-item">
          <a class="nav-link active" href="#wr" role="tab" data-toggle="tab" aria-controls="ci">Writer's Info</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#pr" role="tab" data-toggle="tab" aria-controls="pr">Prompts</a>
        </li>
      </ul>
    </div>
    <div class="" style="height: 10px;"></div>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane active fade in" id="wr">
        @include('errors.error_list')
        @include('writers.inc_tab-wr')
      </div>
      <div role="tabpanel" class="tab-pane fade" id="pr">
        @include('writers.inc_tab-pr')
      </div>
    </div>

    <script>
      $(function () {
        $('#challengeTab a:last').tab('show')
      })
    </script>
@endsection
