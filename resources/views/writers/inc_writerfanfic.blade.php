<fieldset>
    <legend>Link to {{ $writer->name }} on FanFic sites</legend>
    {{--
        <ul>
        @if(count($writer->fanfics) > 0)
            @foreach( $writer->fanfics as $fanfic )
                <li>{{ $fanfic->link_fanfic }}</li>
            @endforeach
        @endif
        </ul>
    --}}
        @if( count($writer->fanfics) > 0 )
            @foreach( $writer->fanfics as $link )
            <!-- site_writer form field -->
            <div class="form-group row">
                {!! Form::label('link_fanfic', '&nbsp;', ['class' => 'col-sm-2']) !!}
                <div class="col-sm-8 input-group">
                  {!! Form::text('link_fanfic['.$link->id.']', $link->link_fanfic, ['class' => 'form-control col-sm-8', 'id' => 'site_writer']) !!}
                  <span class="input-group-addon">
                    <a href="{{ $link->link_fanfic }}" target="_blank"><span class="icon icon-outsidelink"></span></a>
                  </span>
                </div>
            </div>
            @endforeach
        @endif

        <!-- link_fanfic form field -->
        <div class="form-group row">
            {!! Form::label('new_fanfic', '&nbsp;', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::text('new_fanfic', null, ['class' => 'form-control col-sm-8', 'id' => 'new_fanfic', 'placeholder' => 'Add writer\'s fanfic link here']) !!}
            </div>
        </div>
</fieldset>

