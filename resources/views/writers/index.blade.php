@extends('layout')

@section('content')
    <h1>Writers
      <small>
        <a href="/writers/create" class="btn btn-sm btn-primary btn-success pull-right hidden-md-down">Add a new writer</a>
        <a href="/writers/create" class="btn btn-sm btn-primary btn-success pull-right hidden-sm-up">New</a>
      </small>
    </h1>
    <hr />

    @if( count($active) > 0 )
      <div class="tab-content">
        <ul class="nav nav-tabs" role="tablist" id="writersTab">
          <li class="nav-item">
            <a class="nav-link active" href="#active" role="tab" data-toggle="tab" aria-controls="active">
              <span class="icon icon-active"></span> Active Writers
              <span class="label label-pill label-primary">{{ count( $active) }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#inactive" role="tab" data-toggle="tab" aria-controls="inactive">
              <span class="icon icon-inactive"></span> Inactive Writers
              <span class="label label-pill label-info">{{ count( $inactive) }}</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#archived" role="tab" data-toggle="tab" aria-controls="archive">
              <span class="icon icon-archive"></span> Archived Writers
              <span class="label label-pill label-default">{{ count( $archived) }}</span>
            </a>
          </li>
        </ul>
      </div>
      <div class="" style="height: 10px;"></div>

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active fade in" id="active">
          @include('writers.inc_list-writers-table', array( 'writers' => $active ))
        </div>
        <div role="tabpanel" class="tab-pane fade" id="inactive">
          @include('writers.inc_list-writers-table', array( 'writers' => $inactive ))
        </div>
        <div role="tabpanel" class="tab-pane fade" id="archived">
          @include('writers.inc_list-writers-table', array( 'writers' => $archived ))
        </div>
      </div>
    @else
        <p>We don't have any writers yet. Please <a href="/writers/create">add one</a>.</p>
    @endif

@endsection
