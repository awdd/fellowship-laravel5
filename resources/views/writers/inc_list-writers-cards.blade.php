<div class="row">
  @foreach( $writers as $writer )
  <div class="col-sm-4">
    <div class="card status_{{ $writer->flag_status }}">
      <div class="card-block">
        <a href="/writers/{{ $writer->id }}/edit">{{ $writer->name }}</a>
        <div class="pull-right">
          @if( $writer->flag_goat == 1)<span class="icon icon-goat" title="Goat"></span>@endif
          <span class="icon icon-{{ $writer->flag_nsfw }}" title="{{ $writer->flag_nsfw }}"></span>
          <span class="icon icon-{{ $writer->flag_status }}" title="{{ $writer->flag_status }}"></span>
        </div>
      </div>
        <ul class="list-group">
          <li class="list-group-item">
            AU : {{ $writer->flag_au }} |
            DLC : {{ $writer->flag_dlc }}
          </li>
        </ul>
      <div class="card-block">
        {{ $writer->notes }}
      </div>
      <div class="card-block">
        <div class="col-sm-6">
          <a href="{{ $writer->link_facebook }}" target="_blank" class="btn btn-sm btn-primary btn-info">FaceBook</a>
        </div>

        @if($writer->flag_status != 'archive')
        {!! Form::model($writer, ['method' => 'DELETE', 'action' => ['WritersController@destroy', $writer->id]]) !!}
          <!-- Archive Form Submit Button -->
        <div class="col-sm-6">
          {!! Form::submit('Archive', ['class' => 'btn btn-sm btn-primary btn-danger form-control']) !!}
        </div>
        {!! Form::close() !!}
        @endif
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  @endforeach
</div>
