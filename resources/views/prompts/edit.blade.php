@extends('layout')

@section('content')
    <h1>Add a New Prompt</h1>

    @include('errors.error_list')

    {!! Form::model($prompts, ['method' => 'PATCH', 'action' => ['PromptsController@update', $prompts->id], 'class' => 'form form-horizontal']) !!}

    <fieldset>
        <legend>Prompt Info</legend>
        <!-- prompter Form Field -->
        <div class="form-group row">
            {!! Form::label('name', 'Prompter:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Who submited the prompt']) !!}
            </div>
        </div>
        <!-- prompt Form Field -->
        <div class="form-group row">
            {!! Form::label('prompt', 'Prompt:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('prompt', null, ['class' => 'form-control col-sm-8', 'id' => 'prompt', 'placeholder' => 'Prompt']) !!}
            </div>
        </div>
        <!-- posted Form Field -->
        <div class="form-group row">
            {!! Form::label('flag_archive', 'Archive:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-1">
                {!! Form::checkbox('flag_archive', null, ['class' => 'form-control col-sm-1', 'id' => 'flag_archive']) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('flag_nsfw', 'NSFW:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::select('flag_nsfw', $prompts->getNsfw(), $prompts->flag_nsfw, ['class' => 'form-control col-sm-8 c-select']) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('flag_au', 'AU:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::select('flag_au',$prompts->getAu(), $prompts->flag_au, ['class' => 'form-control col-sm-8 c-select']) !!}
            </div>
        </div>

    </fieldset>

    <fieldset>
        <legend>Challenge Info</legend>
        <div class="form-group row">
            {!! Form::label('challenge_id', 'Challenge:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8 input-group">
                {!! Form::select('challenges_id',$prompts->getChallengeList(), $prompts->challenges_id, ['class' => 'form-control col-sm-8 c-select']) !!}
              <span class="input-group-addon"><a href="/challenges/{{ $prompts->challenges_id }}/edit"><span class="icon icon-challenge"></span></a></span>
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('writer_id', 'Writer:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8 input-group">
              {!! Form::select('writer_id',$prompts->getWriterList(), $prompts->writer_id, ['class' => 'form-control col-sm-8 c-select']) !!}
              <span class="input-group-addon"><a href="/writers/{{ $prompts->writer_id }}/edit"><span class="icon icon-writer"></span></a></span>
            </div>
        </div>
        <!-- date_complete Form Field -->
        <div class="form-group row">
            {!! Form::label('date_completed', 'Completed:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::input('date','date_completed', $prompts->date_complete, ['class' => 'form-control col-sm-8', 'id' => 'date_completed', 'placeholder' => 'YYYY-MM-DD']) !!}
            </div>
        </div>
        <!-- prompt Form Field -->
        <div class="form-group row">
            {!! Form::label('link_prompt', 'Prompt Link:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8 input-group">
                {!! Form::text('link_prompt', null, ['class' => 'form-control col-sm-8', 'id' => 'link_prompt', 'placeholder' => 'Prompt Link']) !!}
              <span class="input-group-addon">
                @if( $prompts->link_prompt )
                  <a href="{{ $prompts->link_prompt }}" target="_blank"><span class="icon icon-outsidelink"></span></a>
                @endif
              </span>
            </div>
        </div>
        <!-- posted Form Field -->
        <div class="form-group row">
            {!! Form::label('flag_posted', 'Posted:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-1">
                {!! Form::checkbox('flag_posted', null, ['class' => 'form-control col-sm-1', 'id' => 'flag_posted']) !!}
            </div>
        </div>
    </fieldset>

    <!-- Add a new prompt Form Submit Button -->
    <div class="form-group row">
        <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Update the prompt', ['class' => 'btn btn-primary form-control']) !!}</div>
    </div>


    {!! Form::close() !!}
@endsection
