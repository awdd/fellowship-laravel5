@extends('layout')

@section('content')
    <h1>Add a New Prompt</h1>

    @include('errors.error_list')

    {!! Form::open(['url' => 'prompts', 'class' => 'form form-horizontal']) !!}

    <fieldset>
        <legend>Prompt Info <small>Default information for the prompt</small></legend>
        <!-- prompter Form Field -->
        <div class="form-group row">
            {!! Form::label('name', 'Prompter:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Who submited the prompt']) !!}
            </div>
        </div>
        <!-- prompt Form Field -->
        <div class="form-group row">
            {!! Form::label('prompt', 'Prompt:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::textarea('prompt', null, ['class' => 'form-control col-sm-8', 'id' => 'prompt', 'placeholder' => 'Prompt']) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('flag_nsfw', 'NSFW:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::select('flag_nsfw', $prompt->getNsfw(), 'Either', ['class' => 'form-control col-sm-8']) !!}
            </div>
        </div>
        <div class="form-group row">
            {!! Form::label('flag_au', 'AU:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::select('flag_au',$prompt->getAu(), 'No', ['class' => 'form-control col-sm-8']) !!}
            </div>
        </div>

    </fieldset>

    <fieldset>
        <legend>Prompt Options <small>If we are adding this to a specific challenge</small></legend>
        <div class="form-group row">
            {!! Form::label('challenge_id', 'Challenge:', ['class' => 'col-sm-2']) !!}
            <div class="col-sm-8">
                {!! Form::select('challenges_id',$prompt->getChallengeList(), null, ['class' => 'form-control col-sm-8']) !!}
            </div>
        </div>
    </fieldset>

    <!-- Add a new prompt Form Submit Button -->
    <div class="form-group row">
        <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Add a new prompt', ['class' => 'btn btn-primary form-control']) !!}</div>
    </div>


    {!! Form::close() !!}
@endsection
