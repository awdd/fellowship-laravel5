@if( count($prompts) > 0 )
    <div class="table-responsive">
    <table class="table table-hover table-condensed ">
          <thead>
            <tr>
              <th>Prompter</th>
              <th class="hidden-sm-diwn">NSFW | AU</th>
              <th>Challenge</th>
              <th>Writer</th>
              <th>Completed</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          @foreach( $prompts as $prompt )
            @include('prompts.includes.tr-prompt')
          @endforeach
          </tbody>
      </table>
    </div>
@else
    <p>We do not have any prompts yet matching your criteria. Feel free to <a href="/prompts/create">add one.</a></p>
@endif
