@if( count($prompts) > 0 )
<div class="list-group">
  @foreach( $prompts as $prompt )
    @include('prompts.includes.lg-prompts')
  @endforeach
</div>
@else
    <p>We do not have any prompts yet matching your criteria. Feel free to <a href="/prompts/create">add one.</a></p>
@endif
