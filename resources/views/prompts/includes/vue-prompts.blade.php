<div class="list-group">
    <ul>
        <li class="list-group-item @{{ flag_nsfw }} row" v-repeat="prompts">
            <div class="col-sm-11">

                    <div class="row">
                        <div class="col-sm-12"><strong>Prompt: </strong> @{{ prompt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4"><strong>Prompter: </strong> @{{ name }}</div>
                        <div class="col-sm-8">
                            <strong>Flags: @{{ flag_nsfw }} | @{{ flag_au }}</strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <strong>Challenge: </strong>
                        </div>
                        <div class="col-sm-4">
                            <strong>Writer: </strong>
                        </div>
                        <div class="col-sm-4">
                            <strong>Completed: </strong>
                        </div>
                    </div>

            </div>
            <div class="col-sm-1">
                <a href="/prompts/@{{ id }}/edit"><span class="icon icon-prompts"></span></a><br />
                <span class="icon icon-@{{ flag_nsfw }}"></span><br />

            </div>
        </li>
    </ul>
</div>
