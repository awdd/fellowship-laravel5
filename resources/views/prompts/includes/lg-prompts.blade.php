<a href="/prompts/{{ $prompt->id }}/edit" class="list-group-item {{ $prompt->flag_nsfw }} clearfix @if( $prompt->date_completed != '0000-00-00' )status_inactive @endif">
  <h6 class="list-group-item-heading">
    <span class="icon icon-{{ $prompt->flag_nsfw }} pull-right"></span>
    {{ $prompt->prompt }}
  </h6>
  <div class="row">
    <dl class="list-group-item-text">
      <dt class="col-sm-2">Prompter:</dt>
      <dd class="col-sm-2">{{ $prompt->name }}</dd>
    </dl>
    <dt class="col-sm-2">Flags:</dt>
    <dd class="col-sm-2">{{ $prompt->flag_nsfw }} | {{ $prompt->flag_au }}</dd>
      <dt class="col-sm-2">Posted:</dt>
      <dd class="col-sm-2">
          @if( $prompt->flag_posted != '')
              <span class="icon icon-tick"></span>
          @endif
      </dd>
  </div>
  <div class="row">
    <dl class="list-group-item-text">
      <dt class="col-sm-2">Challenge:</dt>
      <dd class="col-sm-2">
        @if( ! $prompt->challenges_id ) <span class="text-warning">Unassigned</span>
        @else {{ $prompt->challenges->name }}
        @endif
      </dd>
    <dt class="col-sm-2">Writer:</dt>
      <dd class="col-sm-2">
        @if( ! $prompt->writer_id ) <span class="text-warning">Unassigned</span>
        @else {{ $prompt->writer->name }}
        @endif
      </dd>
      <dt class="col-sm-2">Completed:</dt>
      <dd class="col-sm-2">
        @if( $prompt->date_completed != '0000-00-00')
        {{ $prompt->date_completed }}
          @endif
      </dd>
    </dl>
  </div>
</a>
