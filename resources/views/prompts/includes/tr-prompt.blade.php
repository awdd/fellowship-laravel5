<tr>
  <td colspan="6"><a href="/prompts/{{ $prompt->id }}/edit" target="{{ $target }}">{{ $prompt->prompt }}</a></td>
</tr>
<tr class="no-border">
  <td>{{ $prompt->name }}</td>
  <td class="hidden-sm-down">
    {{ $prompt->flag_nsfw }} |
    {{ $prompt->flag_au }}
  </td>
  <td>
    @if( $prompt->challenges_id == '')
      Not assigned
    @else
      {{ $prompt->challenges->name }}
    @endif
  </td>
  <td class="@if( $prompt->writer_id == '') warning @endif ">
    @if( $prompt->writer_id == '')
      Not assigned
    @else
      {{ $prompt->writer->name }}
    @endif
  </td>
  <td>@if( ! $prompt->date_complete )@else{{ $prompt->date_completed }}@endif</td>
  <td class="align-center">
    <a href="/prompts/{{ $prompt->id }}/edit" target="{{ $target }}"><span class="icon icon-pencil" aria-hidden="true"></span></a>
  </td>
</tr>
