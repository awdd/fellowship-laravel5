@extends('layout')

@section('pageid', 'prompts')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1>Un-Assigned Prompts
                <!-- Split button -->
                <small>
                    <a href="/prompts/create" class="btn btn-primary btn-success btn-sm pull-right hidden-md-down">New prompts</a>
                    <a href="/prompts/create" class="btn btn-primary btn-success btn-sm pull-right hidden-sm-up">New</a>
                </small>
            </h1>
        </div>
    </div>
    <div class="clear-fix"></div>
    <hr />
    @include('prompts.includes.lg-prompts-full')

@endsection
