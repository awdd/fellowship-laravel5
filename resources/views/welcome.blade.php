@extends('layout')

@section('content')
    <h1>Fellowship of Cullenite Writers</h1>
    <div>
        Welcome to our admin-y site. This will grow as I can make things work the way that I want them to.<br /><br />
    </div>
    <div>
        If you are interested in writing fan-fiction based on Dragon Age (owned by BioWare) and featuring Cullen (hubba hubba), than please feel free to visit our <a href="http://www.facebook.com/groups/763050627145195">Facebook page</a>. Please note that we are a sub-group of the private <a href="http://www.facebook.com/groups/Cullenites">Cullenites Facebook group</a>, so you will need to be a member of that group before you can join the Fellowship of Cullenite Writers. <br /><br />
        We feature writing challenges, large and small, so if you think you're up to it, swing by and say hello!
    </div>
@stop