<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Fellowship of Cullenite Writers</title>
  <link href="/css/libs/select2.min.css" rel="stylesheet" />
    <link href="/css/libs/sweetalert2.css" rel="stylesheet" />
    {{-- bootstrap 4 alpha --}}
  <link rel="stylesheet" href="/css/libs/bootstrap.css">
  <link rel="stylesheet" href="/css/style.css" media="screen" />
</head>
<body>
  @include('partials.inc_nav4')

  <div class="container @yield('pageid')" id="@yield('pageid')">
@yield('content')
  </div>

<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
  <script src="/js/libs/jquery.js"></script>
  <script src="/js/libs/select2.min.js"></script>
  <script src="/js/libs/sweetalert2.min.js"></script>
  {{-- bootstrap 4 alpha --}}
  <script src="/js/libs/bootstrap.js"></script>
  @include('flash')
  @yield('footer')
</body>

</html>
