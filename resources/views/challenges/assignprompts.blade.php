@extends('layout')

@section('content')
<div class="row">
    <div class="notes">
        You can keep this open in a seperate window while assigning prompts. Refreshing will re-order the prompts and writers.
    </div>
    <hr />
    <div class="col-md-6">
        <h3>"NSFW" Prompts</h3>
        <ol>
            @foreach( $prompts as $prompt )
                @if( $prompt->flag_nsfw == 'NSFW' )
                    <li>{{ str_limit( $prompt->prompt, $limit="50", $end="...") }} <small>by {{ $prompt->name }}</small></li>
                @endif
            @endforeach
        </ol>
    </div>
    <div class="col-md-6">
        <h3>"NSFW" Writers</h3>
        <ol>
            @foreach( $writers as $writer )
                @if( $writer->flag_nsfw == "NSFW" )
                    <li>{{ $writer->name }} <small>[
                        <strong>AU?</strong> {{ $writer->flag_au }} |
                        <strong>DLC?</strong> {{ $writer->flag_dlc }} |
                        <strong>Notes: </strong> {{ $writer->notes }} |
                        ]</small></li>
                @endif
            @endforeach
        </ol>
    </div>
</div>
<hr />

<div class="row">
    <div class="col-md-6">
        <h3>"SFW" Prompts</h3>
        <ol>
            @foreach( $prompts as $prompt )
                @if( $prompt->flag_nsfw == 'SFW' )
                    <li>{{ str_limit( $prompt->prompt, $limit="50", $end="...") }} <small>by {{ $prompt->name }}</small></li>
                @endif
            @endforeach
        </ol>
    </div>
    <div class="col-md-6">
        <h3>"SFW" Writers</h3>
        <ol>
            @foreach( $writers as $writer )
                @if( $writer->flag_nsfw == "SFW" )
                    <li>{{ $writer->name }} <small>[
                            <strong>AU?</strong> {{ $writer->flag_au }} |
                            <strong>DLC?</strong> {{ $writer->flag_dlc }} |
                            <strong>Notes: </strong> {{ $writer->notes }} |
                            ]</small></li>
                @endif
            @endforeach
        </ol>
    </div>
</div>
<hr />

<div class="row">
    <div class="col-md-6">
        <h3>"Either" Prompts</h3>
        <ol>
            @foreach( $prompts as $prompt )
                @if( $prompt->flag_nsfw == 'Either' )
                    <li>{{ str_limit( $prompt->prompt, $limit="50", $end="...") }} <small>by {{ $prompt->name }}</small></li>
                @endif
            @endforeach
        </ol>
    </div>
    <div class="col-md-6">
        <h3>"Either" Writers</h3>
        <ol>
            @foreach( $writers as $writer )
                @if( $writer->flag_nsfw == "Either" )
                    <li>{{ $writer->name }} <small>[
                            <strong>AU?</strong> {{ $writer->flag_au }} |
                            <strong>DLC?</strong> {{ $writer->flag_dlc }} |
                            <strong>Notes: </strong> {{ $writer->notes }} |
                            ]</small></li>
                @endif
            @endforeach
        </ol>
    </div>
</div>
<hr />
@endsection


@section('footer')
    @parent
@endsection
