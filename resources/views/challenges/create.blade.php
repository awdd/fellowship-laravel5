@extends('layout')

@section('content')
  <h1>Start a new Challenge</h1>

  @include('errors.error_list')

  {!! Form::open(['url' => 'challenges', 'class' => 'form ']) !!}
    <!-- name form field -->
    <div class="form-group row">
      {!! Form::label('name', 'Name:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control col-sm-8', 'id' => 'name', 'placeholder' => 'Enter a name for the challenge']) !!}
      </div>
    </div>
    <!-- flag_type form field -->
    <div class="form-group row">
      {!! Form::label('flag_type', 'Challenge Type :', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_type', ['seasonal' => 'Seasonal', 'weekly' => 'Weekly', 'monthly' => 'Monthly', 'random' => 'Random'], null, ['class' => 'form-control col-sm-8', 'id' => 'flag_type']) !!}
      </div>
    </div>
    <!-- date_start form field -->
    <div class="form-group row">
      {!! Form::label('date_start', 'Start Date:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::input('date','date_start', date('Y-m-d'), ['class' => 'form-control col-sm-8', 'id' => 'date_start', 'placeholder' => 'YYYY-MM-DD']) !!}
      </div>
    </div>
    <!-- date_end form field -->
    <div class="form-group row">
      {!! Form::label('date_end', 'End Date:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::input('date', 'date_end', date('Y-m-d'), ['class' => 'form-control col-sm-8', 'id' => 'date_end', 'placeholder' => 'YYYY-MM-DD']) !!}
      </div>
    </div>
    <!-- description form field -->
    <div class="form-group row">
      {!! Form::label('description', 'Description:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('description', null, ['class' => 'form-control col-sm-8', 'id' => 'description', 'placeholder' => 'Description']) !!}
      </div>
    </div>

    <!-- Add A New Challenge Form Submit Button -->
    <div class="form-group row">
      <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Start Challenge', ['class' => 'btn btn-primary form-control']) !!}</div>
    </div>

  {!! Form::close() !!}

@endsection
