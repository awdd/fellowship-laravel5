@extends('layout')

@section('content')
  <h1>Challenges
    <small>
      <a href="/challenges/create" class="btn btn-primary btn-success btn-sm pull-right hidden-md-down">New Challenge</a>
      <a href="/challenges/create" class="btn btn-primary btn-success btn-sm pull-right hidden-sm-up">New</a>
    </small>
  </h1>
  <hr />

  @if( count($challenges) > 0 )
  <table class="table table-responsive table-condensed table-hover">
    <thead>
    <tr>
      <th class="">Status</th>
      <th class="">Challenge Name</th>
      <th class="">Start Date</th>
      <th class="">End Date</th>
      <th class="hidden-sm-down">Type</th>
      <th class="hidden-md-down">Description</th>
      <th class="">Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($challenges as $challenge)
      <tr>
        <td class="status_{{ $challenge->flag_status }}">{{ $challenge->getHumanStatus($challenge->flag_status) }}</td>
        <td class="status_{{ $challenge->flag_status }}">
          <a href="/challenges/{{ $challenge->id }}/edit">{{ $challenge->name }}</a>
        </td>
        <td class="status_{{ $challenge->flag_status }}">{{ $challenge->date_start }}</td>
        <td class="status_{{ $challenge->flag_status }}">{{ $challenge->date_end }}</td>
        <td class="status_{{ $challenge->flag_status }} hidden-sm-down">
          {{ $challenge->getHumanType($challenge->flag_type) }}
        </td>
        <td class="status_{{ $challenge->flag_status }} hidden-md-down">{{ $challenge->description }}</td>
        <td
          @if( $challenge->flag_status == 'active') class="status_archive"
          @elseif( $challenge->flag_status == 'inactive') class="status_active"
          @endif
        >
          {!! Form::model($challenge, ['method' => 'DELETE', 'action' => ['ChallengesController@destroy', $challenge->id], 'id' => $challenge->id]) !!}
          <div>
            {!! Form::submit('Archive', ['class' => 'btn btn-sm btn-primary btn-danger form-control']) !!}
          </div>
          {!! Form::close() !!}
        </td>
      </tr>
    @endforeach
    </tbody>
  </table>
  @else
    <p>We currently don't have any challenges in the system. Please <a href="/challenges/create">add one</a></p>
  @endif
@endsection
