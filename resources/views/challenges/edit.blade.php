@extends('layout')

@section('content')
  <h1>Update {{ $challenges->name }}</h1>

  <div class="tab-content">
    <ul class="nav nav-tabs" role="tablist" id="challengeTab">
    <li class="nav-item">
      <a class="nav-link active" href="#ci" role="tab" data-toggle="tab" aria-controls="ci">Challenge Info</a>
    </li>
    <li class="nav-item">
        <a class="nav-link disabled" href="#ci" role="tab">Prompts:</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#pr1" role="tab" data-toggle="tab" aria-controls="pr1">Not assigned out <span class="label label-warning label-pill">{{ count( $pbdn['no_writer']) }}</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#pr2" role="tab" data-toggle="tab" aria-controls="pr2">In progress <span class="label label-info label-pill">{{ count( $pbdn['incom']) }}</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#pr3" role="tab" data-toggle="tab" aria-controls="pr3">Completed <span class="label label-success label-pill">{{ count( $pbdn['com']) }}</span></a>
    </li>
  </ul>
  </div>
  <div class="" style="height: 10px;"></div>

  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active fade in" id="ci">
      @include('errors.error_list')
      @include('challenges.inc_tab-ci')
    </div>
      {{-- Prompt tabs --}}
    <div role="tabpanel" class="tab-pane fade" id="pr">
      @include('challenges.inc_tab-pr', array('prompts' => $challenges->prompts ))
    </div>
      <div role="tabpanel" class="tab-pane fade" id="pr1">
          <div class="clearfix">
              <a href="/challenges/{{ $challenges->id }}/assignprompts" class="btn btn-success pull-left" target="_blank">Assign prompts</a>
              <a href="/prompts" class="btn btn-secondary pull-right">Add more prompts</a>
          </div>
          <div class="notes">Right now, I can't figure out how to redirect back here when editing a challenge from here. To get around that, you can right-click and open the prompts in new tabs/windows to assign, or edit in the same window and click back to this challenge.</div>
          <hr />
          @include('challenges.inc_tab-pr', array( 'prompts' => $pbdn['no_writer'] ))
      </div>
      <div role="tabpanel" class="tab-pane fade" id="pr2">
          @include('challenges.inc_tab-pr', array( 'prompts' => $pbdn['incom'] ))
      </div>
      <div role="tabpanel" class="tab-pane fade" id="pr3">
          @include('challenges.inc_tab-pr', array( 'prompts' => $pbdn['com'] ))
      </div>
  </div>
  <script>
    $(function () {
      $('#challengeTab a:last').tab('show')
    })
  </script>


@endsection

@section('footer')
  @parent
@endsection
