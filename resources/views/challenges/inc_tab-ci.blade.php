{!! Form::model($challenges, ['method' => 'PATCH', 'action' => ['ChallengesController@update', $challenges->id], 'class' => 'form form-horizontal']) !!}
  <div class="col-sm-9">
      <!-- flag_status form field -->
    <div class="form-group row">
      {!! Form::label('name', 'Status:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_status', $challenges->getStatus(), $challenges->flag_status, ['class' => 'form-control col-sm-8 c-select', 'id' => 'flag_status']) !!}
      </div>
    </div>
    <!-- name form field -->
    <div class="form-group row">
      {!! Form::label('name', 'Name:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control col-sm-8', 'id' => 'name', 'placeholder' => 'Enter a name for the challenge']) !!}
      </div>
    </div>
    <!-- flag_type form field -->
    <div class="form-group row">
      {!! Form::label('flag_type', 'Challenge Type :', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::select('flag_type', $challenges->getType(), $challenges->flag_type, ['class' => 'form-control col-sm-8 c-select', 'id' => 'flag_type']) !!}
      </div>
    </div>
    <!-- date_start form field -->
    <div class="form-group row">
      {!! Form::label('date_start', 'Start Date:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::input('date','date_start', $challenges->date_start, ['class' => 'form-control col-sm-8', 'id' => 'date_start', 'placeholder' => 'YYYY-MM-DD']) !!}
      </div>
    </div>
    <!-- date_end form field -->
    <div class="form-group row">
      {!! Form::label('date_end', 'End Date:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::input('date', 'date_end', $challenges->date_end, ['class' => 'form-control col-sm-8', 'id' => 'date_end', 'placeholder' => 'YYYY-MM-DD']) !!}
      </div>
    </div>
    <!-- description form field -->
    <div class="form-group row">
      {!! Form::label('description', 'Description:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::textarea('description', null, ['class' => 'form-control col-sm-8', 'id' => 'description', 'placeholder' => 'Description']) !!}
      </div>
    </div>

    <!-- Update the Challenge Form Submit Button -->
    <div class="form-group row">
      <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Update the Challenge', ['class' => 'btn btn-primary form-control']) !!}</div>
    </div>
  </div>
<div class="col-sm-3">
  <div class="form-group row">
    {!! Form::label('writers_list', 'Writers Participating', ['class' => '']) !!} <span class="label label-info label-pill">{{ count( $challenges->writer ) }}</span>
    {!! Form::select('writers_list[]', $writers_list, null, ['class' => 'form-control', 'id' => 'writers_list', 'multiple']) !!}
  </div>

</div>
{!! Form::close() !!}

@section('footer')
  @parent
  <script type="text/javascript">
    $('#writers_list').select2();
  </script>
@endsection
