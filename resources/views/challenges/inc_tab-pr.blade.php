@if( $prompts )
  <div class="table-responsive">
    <table class="table table-hover table-condensed ">
  @foreach( $prompts as $prompt )
      @include('prompts.includes.lg-prompts')
  @endforeach
    </table>
  </div>
@endif
