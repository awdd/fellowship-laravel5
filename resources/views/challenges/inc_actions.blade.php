@if($challenge->flag_status == 'prep')
  {!! Form::model($challenge, ['method' => 'PATCH', 'action' => ['ChallengesController@update', $challenge->id], 'id' => $challenge->id]) !!}
    {!! Form::hidden('flag_status', 'prompt_request') !!}
  <div>
    {!! Form::submit('Prompt Request', ['class' => 'btn btn-sm btn-primary btn-info form-control']) !!}
  </div>
  {!! Form::close() !!}

@elseif($challenge->flag_status == 'complete')
  {!! Form::model($challenge, ['method' => 'DELETE', 'action' => ['ChallengesController@destroy', $challenge->id], 'id' => $challenge->id]) !!}
  <div>
    {!! Form::submit('Archive', ['class' => 'btn btn-sm btn-primary btn-danger form-control']) !!}
  </div>
  {!! Form::close() !!}
@endif
