    <!-- name form field -->
    <div class="form-group row">
      {!! Form::label('name', 'Site Name:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('name', null, ['class' => 'form-control col-sm-8', 'id' => 'name', 'placeholder' => 'Add the site name here']) !!}
      </div>
    </div>

    <!-- link_main form field -->
    <div class="form-group row">
      {!! Form::label('link_main', 'Site Homepage:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('link_main', null, ['class' => 'form-control col-sm-8', 'id' => 'link_main', 'placeholder' => 'Homepage of the site']) !!}
      </div>
    </div>
    <!-- link_user_prefix form field -->
    <div class="form-group row">
      {!! Form::label('link_user_prefix', 'User Prefix:', ['class' => 'col-sm-2']) !!}
      <div class="col-sm-8">
        {!! Form::text('link_user_prefix', null, ['class' => 'form-control col-sm-8', 'id' => 'link_user_prefix', 'placeholder' => 'User Prefix']) !!}
      </div>
    </div>
