@extends('layout')

@section('content')
<h1>Add A FanFic Site</h1>

  @include('errors.error_list')

  {!! Form::open(['url' => 'sites', 'class' => 'form form-horizontal']) !!}

  @include('sites.inc_addedit')
  <!-- Add A New FanFic Site Form Submit Button -->
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Add A New FanFic Site', ['class' => 'btn btn-primary form-control']) !!}</div>
  </div>

{!! Form::close() !!}
@endsection
