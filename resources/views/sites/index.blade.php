@extends('layout')

@section('content')
  <h1>Manage FanFic Sites <small><a href="/sites/create" class="btn btn-primary btn-success pull-right">Add A New Site</a></small></h1>

  @if( count($sites) > 0 )
  <table class="table table-hover table-striped table-condensed table-bordered">
    <thead>
      <tr>
        <th>Site Name</th>
        <th>Site Link</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      @foreach( $sites as $site )
        <tr>
          <td>
            <a href="/sites/{{ $site->id }}/edit">{{ $site->name }}</a>
          </td>
          <td>
            <a href="{{ $site->link_main }}" target="_blank">{{ $site->link_main }}</a>
          </td>
          <td>
            &nbsp;
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
  @else
    <p>We don't have any fanfic sites listed yet. Please <a href="/sites/create">add one</a>.</p>
  @endif
@endsection
