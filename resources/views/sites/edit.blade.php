@extends('layout')

@section('content')
  <h1>Update A FanFic Site</h1>

  @include('errors.error_list')

  {!! Form::model($site, ['method' => 'PATCH', 'action' => ['WritingSitesController@update', $site->id], 'class' => 'form form-horizontal']) !!}

  @include('sites.inc_addedit')
    <!-- Update FanFic Site Form Submit Button -->
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-8">{!! Form::submit('Update FanFic Site', ['class' => 'btn btn-primary form-control']) !!}</div>
  </div>

  {!! Form::close() !!}
@endsection
