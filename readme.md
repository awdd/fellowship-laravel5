## Fellowship of Cullenite Writers

This was designed to help the admins of the Facebook Group - Fellowship of Cullenite Writers - manage the challenges.

Writers could be added, edited and archived. Their name, facebok link and links to outside content (fanfiction profiles) could be added and edited. Archiving a writer removed them from the list of available writers for a challenge. We could also store their prompt preferences (sfw/nsfw/both, alternate universe or not) as well as notes specific to that writer.

Challenges could be added and set up to be either a short&quick challenge, or the longer challenge. Prompts could be associated to a challenge and a writer, with the date completed and a link to the prompt.

Prompts could be added and left unassigned or assigned to a challenge. Prompt information includes prompter, prompt info and rating (sfw/not sfw/both).

Most things had colors and icons to designate flags and status.