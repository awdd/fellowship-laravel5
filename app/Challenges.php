<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use MartinBean\Database\Eloquent\Sluggable;

class Challenges extends Model
{

  use Sluggable;

  protected $status = [
    'Pre' => ['prep' => 'Prep', 'prompt_request' => 'Prompt request'],
    'Current' => ['writing' => 'Writing'],
    'Post' => ['complete' => 'Complete', 'archive' => 'Archive']
  ];
  protected $type = ['seasonal' => 'Seasonal', 'weekly' => 'Weekly', 'monthly' => 'Monthly', 'random' => 'Random'];

  public function getStatus()
  {
    return $this->status;
  }

  public function getHumanStatus($status)
  {
    $status = ucfirst(str_replace('_', ' ', $status));
    return $status;
  }

  public function getType()
  {
    return $this->type;
  }

  public function getHumanType($type)
  {
    $type = ucfirst(str_replace('_', ' ', $type));
    return $type;
  }

  protected $fillable = [
    'name', 'description', 'date_start', 'date_end', 'flag_status'
  ];

  protected function setDateStartAttribute($date_start)
  {
    $this->attributes['date_start'] = Carbon::parse($date_start);
  }

  protected function setDateEndAttribute($date_end)
  {
    $this->attributes['date_end'] = Carbon::parse($date_end);
  }

  /* Relationships */
  protected function prompts()
  {
    return $this->hasMany('App\Prompts');
  }

  public function writer()
  {
    return $this->belongsToMany('App\Writer')->withTimestamps();
  }

  protected function getWritersList()
  {
    $list = array('' => 'Not associated with a writer') + Writer::lists('name', 'id');
    return $list;
  }

  protected function getWritersListAttribute()
  {
    return $this->writer->lists('id');
  }
}
