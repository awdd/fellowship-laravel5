<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Prompts extends Model {

  protected $fillable = ['name', 'prompt', 'writer_id', 'flag_nsfw', 'flag_au', 'date_completed', 'link_prompt', 'challenges_id', 'flag_posted', 'flag_archive'];

  protected $nsfw =   ['SFW' => 'SFW', 'NSFW' => 'NSFW', 'Either' => 'Either'];
  protected $au   =   ['No' => 'No', 'AU' => 'AU', 'AU World' => 'AU World', 'AU Time' => 'AU Time'];
  public function getNsfw()   { return $this->nsfw; }
  public function getAu()     { return $this->au; }


  protected function setDateCompleteAttribute($date_completed)
  {
      $this->attributes['date_completed'] = Carbon::parse($date_completed);
  }

  /* Scopes ---------------------------------------------------------------- */
  public function scopeCompleted( $query )
  {
    return $query->where('date_completed', '!=', '0000-00-00')->orderBy('date_completed', 'desc');
  }
  public function scopeIncompleteAssigned( $query )
  {
    return $query->where('date_completed', '=', '0000-00-00');
  }
  public function scopeUnassignedChallenge( $query )
  {
    return $query->where('challenges_id', 'is', '');
  }
  public function scopeUnassignedWriter( $query )
  {
    return $query->where('writer_id', '=', '');
  }
  public function scopeFlagArchive( $query )
  {
    return $query->where( 'flag_archive', '=', '');
  }

  /* Relationships --------------------------------------------------------- */
  public function challenges()
  {
      return $this->belongsTo('App\Challenges');
  }
  public function writer()
  {
      return $this->belongsTo('App\Writer');
  }

  public function getChallengeList()
  {
      $list = array('' => 'Not associated with a challenge') + Challenges::lists('name', 'id');
      return $list;
  }

  public function getWriterList()
  {
      $list = array('' => 'Not associated with a writer') + Writer::lists('name', 'id');
      return $list;
  }

}
