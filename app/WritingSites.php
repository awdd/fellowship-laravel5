<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class WritingSites extends Model {

  protected $fillable = [
    'name', 'link_main', 'link_user_prefix',
  ];


}
