<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use MartinBean\Database\Eloquent\Sluggable;

class Writer extends Model {

  use Sluggable;

  // Attempting this for the enum field values
  protected $goat =   [0 => 'No',1 => 'Yes'];
  protected $nsfw =   ['SFW' => 'SFW', 'NSFW' => 'NSFW', 'Either' => 'Either'];
  protected $au   =   ['No' => 'No', 'AU' => 'AU', 'AU World' => 'AU World', 'AU Time' => 'AU Time'];
  protected $dlc  =   ['No' => 'No', 'All' => 'All', 'Jaws of Hakkon' => 'Jaws of Hakkon', 'Descent' => 'Descent', 'Trespasser' => 'Trespasser'];
  protected $status = ['active' => 'Active', 'inactive' => 'Inactive', 'archive' => 'Archive'];

	protected $fillable = [
        'name', 'link_facebook',
        'flag_goat', 'flag_nsfw', 'flag_au', 'flag_dlc', 'flag_notes', 'flag_status', 'notes'
  ];

  public function getGoat()   { return $this->goat; }
  public function getNsfw()   { return $this->nsfw; }
  public function getAu()     { return $this->au; }
  public function getDlc()    { return $this->dlc; }
  public function getStatus() { return $this->status; }

  /* Scopes -------------------------------------------------------------- */
  public function scopeActive( $query )
  {
   return $query->whereFlagStatus('active');
  }
  public function scopeInactive( $query )
  {
    return $query->whereFlagStatus('inactive');
  }
  public function scopeArchive( $query )
  {
    return $query->whereFlagStatus('archive');
  }

  /* Relationshps -------------------------------------------------------- */
  public function fanfics()
  {
      return $this->hasMany('App\Fanfic');
  }

  public function prompts()
  {
    return $this->hasMany('App\Prompts');
  }

  public function challenges()
  {
    return $this->belongsToMany('App\Challenges')->withTimestamps();
  }

  protected function getChallengesList()
  {
    $list = array('' => 'Not associated with a challenge') + Challenges::lists('name', 'id');
    return $list;
  }
  protected function getChallengesListAttribute()
  {
    return $this->challenges->lists('id');
  }

}
