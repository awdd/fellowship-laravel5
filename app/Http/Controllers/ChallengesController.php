<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Request;
use App\Challenges;
use App\Http\Requests\ChallengesRequest;
use App\Writer;

/*
 * @TODO - Figure out why I can't submit to update from the index list
 *       - challenges/index.blade.php - include the inc_actions.blade.php file in the Actions td
 */

class ChallengesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$challenges = Challenges::all();
        return view('challenges.index', compact('challenges'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('challenges.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(ChallengesRequest $request)
	{
		$input['flag_status'] = 'prep';
		//dd($request->all());
		Challenges::create( $request->all() );
    flash()->success('Huzzah!', 'The challenge has been created!');
		return redirect('challenges');
	}

	/**
	 * Display the specified resource.
	 * @TODO - Do this for front-facing view
	 * @param  Challenges $challenges
	 * @return Response
	 */
	/*
  public function show(Challenges $challenges)
	{
		//
	}
  */
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  Challenges $challenge
	 * @return Response
	 */
	public function edit(Challenges $challenges)
	{
		$target = "_blank";
		$writers_list = Writer::active()->lists('name', 'id');

    if( $challenges->prompts )
    {
      $pbdn = $this->filterPrompts( $challenges->prompts );
    }
    else
    {
      $pbdn = '';
    }

    return view('challenges.edit', compact('challenges', 'target', 'writers_list', 'pbdn'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Challenges $challenges
	 * @return Response
	 */
	public function update(Challenges $challenges, ChallengesRequest $request)
	{
    if( $request->flag_status == 'archive')
    { $this->destroy($challenges); }
    else
    { $challenges->update( $request->all() ); }

    $this->syncWriters( $challenges, $request['writers_list']);

    return redirect('challenges');
	}

  public function assignprompts(Challenges $challenges)
  {
    $tempP = $this->filterPrompts($challenges->prompts);
    $prompts = $tempP['no_writer'];
    shuffle( $prompts );

    $writers = $this->availableWriters($challenges);
    shuffle( $writers );

    return view('challenges.assignprompts', compact('prompts', 'writers'));
  }

  /**
	 * Change the status to Archived - we don't want to delete
	 *
	 * @param  Challenges $challenges
	 * @return Response
	 */
	public function destroy(Challenges $challenges)
	{
        $challenges->flag_status = "archive";
        $challenges->description = 'Archived ' . date("Y-m-d") . " -- " . $challenges->description;
        $challenges->update();
        return redirect('challenges');
	}


  private function syncWriters(Challenges $challenges, array $writer_ids)
  {
    if(count( $writer_ids ) == 0 )
    { // we are dropping everyone from the challenge
      foreach( $challenges->writer as $writer )
      { $challenges->writer()->detach( $writer->id ); }
    }
    else
    { // just update what we need to
      $challenges->writer()->sync( $writer_ids );
    }
  }

  private function filterPrompts( $prompts )
  {
    $pbdn['no_writer'] = array();
    $pbdn['incom'] = array();
    $pbdn['com'] = array();

    if( count($prompts) >= 1 )
    {
      foreach( $prompts as $prompt )
      {
        // check to see if the prompt has been assigned to a writer
        if( $prompt->writer_id == 0 )
        {
          $pbdn['no_writer'][] = $prompt;
        }
        else
        { // it has been assigned , now check to see if the prompt has been completed
          if( $prompt->link_prompt == '' )
          {
            $pbdn['incom'][] = $prompt;
          }
          else
          {
            $pbdn['com'][] = $prompt;
          }
        }
      }
    }
    return $pbdn;
  }

  private function availableWriters( Challenges $challenges )
  {
    foreach( $challenges->writer as $writer )
    {
      if( count( $writer->prompts ) == 0 )
      { // no prompts at all, they are available
        $avail[] = $writer;
      }
      else
      { // the writer is working on or has completed prompts - check for prompts for this challenge
        foreach( $writer->prompts as $prompt )
        {
          if( $prompt->challenges_id == $challenges->id )
          { // the prompt is for this challenge
            if( $prompt->link_prompt != '' )
            { // the was completed, they are available for another
              $avail[] = $writer;
            }
          }
          else
          { // none of their prompts are for this challenge, should be ok
            $avail[] = $writer;
          }
        }
      }
    }

    //dd( $avail );
    return $avail;
  }

}
