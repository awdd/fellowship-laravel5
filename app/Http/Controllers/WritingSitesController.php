<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Request;
use App\WritingSites;
use App\Http\Requests\WritingSitesRequest;

class WritingSitesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$sites = WritingSites::all();
		return view('sites.index', compact('sites'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('sites.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(WritingSitesRequest $request)
	{
		WritingSites::create( $request->all() );
    return redirect('sites');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
  /*
	public function show($id)
	{
		//
	}
  */
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
    $site = WritingSites::findOrFail($id);
		return view('sites.edit', compact('site'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, WritingSitesRequest $request)
	{
    $site = WritingSites::findOrFail($id);
    $site->update($request->all());
		return redirect('sites');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	/*
	public function destroy($id)
	{
		//
	}
	*/
}
