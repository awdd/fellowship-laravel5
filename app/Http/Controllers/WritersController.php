<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\WriterRequest;
use App\Http\Controllers\Controller;

//use Illuminate\Http\Request;
use Request;
use App\Writer;
use App\Fanfic;
use App\Challenges;

class WritersController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// list of writers
    //$writers = Writer::all();
    $active   = Writer::active()->orderBy('name')->get();
    $inactive = Writer::inactive()->orderBy('name')->get();
    $archived = Writer::archive()->orderBy('name')->get();
    return view('writers.index', compact('active', 'inactive', 'archived') );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// form to create a writer
    //flash()->error('Huzzah!', ' has been added as a writer!');
    return view('writers.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(WriterRequest $request)
	{
		// store the new writer's information
		$input['flag_status'] = 'active';
		$input['flag_goat']		= '0';
		$input['flag_nsfw']		= 'Either';
		$input['flag_dlc']		= 'No';
		Writer::create( $request->all() );
    flash()->success('Huzzah!', $request->name . ' has been added as a writer!');
		return redirect('writers');
	}

	/**
	 * Display the specified resource.
	 *
     * TODO - Front-facing, maybe
     *
	 * @param  int  $id
	 * @return Response
	 */
    /*
	public function show($id)
	{
		// show the writer
        $writer = Writer::findOrFail($id);
        return view('writers.show', compact('writer'));
	}
    */
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Writer $writer)
	{
		//dd( $writer->prompts() );
    $challenges_list = Challenges::lists('name','id');
    return view('writers.edit', compact('writer', 'challenges_list'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Writer $writer, WriterRequest $request)
	{
		// store the updated writer information
    // if we have a new fanfic link, we'll need to add and associate it
    if( $request['new_fanfic'] != '')
    {
        $fanfic = new Fanfic();
        $fanfic['writer_id'] = $writer->id;
        $fanfic['link_fanfic'] = $request['new_fanfic'];
        $fanfic->save();
    }
    if( $request['link_fanfic'] != '' )
    {
      foreach( $request['link_fanfic'] as $k => $v)
      {
        if( $v == '')
        { // delete the entry
          $fanfic = Fanfic::findOrFail($k);
          $fanfic->delete();
        }
        else
        {
          $fanfic = Fanfic::findOrFail($k);
          $updatefanfic['id'] = $k;
          $updatefanfic['writer_id'] = $writer->id;
          $updatefanfic['link_fanfic'] = $v;
          $fanfic->update($updatefanfic);
        }
      }
    }
    // sync challenges
    $this->syncChallenges($writer, $request['challenges_list']);


		$writer->update( $request->all() );
    flash()->success('Huzzah!', $request->name . ' has been updated!');
		return redirect('writers');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Writer $writer)
	{
		// archive the writer - don't delete
		$writer->flag_status = 'archive';
		$writer->notes = 'Archived ' . date("Y-m-d") . " -- " . $writer->notes;
		$writer->update();
    flash('Archived!', $writer->name . " has been archived, not deleted. This way we do not lose any of their work.");
		return redirect('writers');
	}

  private function syncChallenges( Writer $writer, $challenge_ids )
  {
    if(count( $challenge_ids ) == 0 )
    {
      foreach( $writer->challenges as $challenge )
      { $writer->challenges()->detach( $challenge->id ); }
    }
    else
    {
      $writer->challenges()->sync( $challenge_ids );
    }
  }

}
