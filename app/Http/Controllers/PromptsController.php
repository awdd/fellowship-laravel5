<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Session\Store as SessionStore;

//use Illuminate\Http\Request;
use App\Prompts;
use App\Http\Requests\PromptsRequest;

class PromptsController extends Controller {

  protected $session;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$prompts = Prompts::UnassignedChallenge()->FlagArchive()->get();
    //dd( $prompts );
		//$prompts = Prompts::UnassignedWriter()->get();
    $target = "_self";
    return view('prompts.index', compact('prompts', 'target'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $prompt = new Prompts();
		return view('prompts.create', compact('prompt'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(PromptsRequest $request)
	{
    Prompts::create($request->all());
    flash()->success('Huzzah!', 'The prompt has been added!');
		return redirect('prompts');
	}

	/**
	 * Display the specified resource.
	 *
     * TODO - Do this for front-facing view
	 * @param  int  $id
	 * @return Response
	 */
    /*
	public function show($id)
	{
		//
	}
    */

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  Prompts $prompts
	 * @return Response
	 */
	public function edit(Prompts $prompts)
	{
    //dd( $prompts );
    return view('prompts.edit', compact('prompts'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Prompts $prompts
	 * @return Response
	 */
	public function update(Prompts $prompts, PromptsRequest $request)
	{
    // if removing the challenge
    if( $request['challenges_id'] == '')
    {
      $prompts->challenges_id = '';
    }

    if( $request['flag_posted'] == "on" )   { $request['flag_posted'] = '1'; }
    if( $request['flag_archive'] == "on" )  { $request['flag_archive'] = '1'; }

    $prompts->update( $request->all() );
    flash()->success('Huzzah!', 'The prompt has been updated!');
		return redirect('prompts');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Prompts $prompts
	 * @return Response
	 */
    /*
	public function destroy(Prompts $prompts)
	{
        // needs to be submitted as a form - bleh
        return redirect('prompts');
	}
    */
}
