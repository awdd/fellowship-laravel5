<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('writers', 'WritersController');
Route::get('challenges/{challenges}/assignprompts', 'ChallengesController@assignprompts');
Route::resource('challenges', 'ChallengesController');
Route::resource('sites', 'WritingSitesController');
Route::resource('prompts', 'PromptsController');
/* Prompts API end points */
Route::get('promptslist', function(){
  return view('prompts.index');
} );
Route::get('apipr', 'PromptsController@apiGet');

Route::get('/', 'WelcomeController@index');
/*
Route::get('contact', 'WelcomeController@contact');
Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
*/

