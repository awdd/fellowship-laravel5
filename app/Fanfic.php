<?php
/**
 * Created by PhpStorm.
 * User: donna.ryan
 * Date: 9/8/2015
 * Time: 2:27 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fanfic extends Model
{
    protected $fillable = ['writer_id', 'link_fanfic'];

    public function writer()
    {
        return $this->belongsTo('App\Writer');
    }
}